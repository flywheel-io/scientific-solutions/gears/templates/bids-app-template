# Update the BIDS algorithm version
1. Fork the repo.
2. [Update the DockerHub image](https://gitlab.com/flywheel-io/scientific-solutions/gears/templates/bids-app-template/-/blob/main/Dockerfile?ref_type=heads#L56) that the gear will use.
3. Update the [gear-builder line](https://gitlab.com/flywheel-io/scientific-solutions/gears/templates/bids-app-template/-/blob/main/manifest.json?ref_type=heads#L48) in the manifest.
4. Update the [version line](https://gitlab.com/flywheel-io/scientific-solutions/gears/templates/bids-app-template/-/blob/main/manifest.json?ref_type=heads#L75) in the manifest.
5. Update the [version](https://gitlab.com/flywheel-io/scientific-solutions/gears/templates/bids-app-template/-/blob/main/pyproject.toml?ref_type=heads#L3) in the pyproject.toml
6. Run `poetry update` from the local commandline (where your cwd is the top-level of the gear). This command will update any dependencies for the Flywheel portion of the gear (not the BIDS algorithm itself!).
7. Run `fw-beta gear build` to update anything in the manifest.
8. Ideally, run `fw-beta gear upload` and complete a test run on your Flywheel instance.
9. Run `git checkout -b {my-update}`, `git commit -a -m "{My message about updating}" -n`, and `git push`.
10. Submit a merge request (MR) to the original gear repo for Flywheel staff to review for official inclusion in the exchange.


# Template Overview 
To create a new BIDS App gear using this template click on “Use this template” above and then edit parser.py, manifest.json, Dockerfile and other files as necessary to create your gear. Most of the changes you need to make are in the manifest.json and Dockerfile (search for “editme” to pinpoint each item). Additionally, replace "bids_app_template" and "bids-app-template" across ALL the files with "bids_{your_algoritm}" and "bids-{your-algorithm}".

Once the edits are complete, we recommend using `fw-beta gear build` to test the Docker configuration and auto-update the manifest. For more on on the build function in `fw-beta`, see https://flywheel-io.gitlab.io/tools/app/cli/fw-beta/gear/build/ 

Many of the python modules are imported from the [BIDS App Toolkit](https://gitlab.com/flywheel-io/public/bids-client/-/tree/master/flywheel_bids/flywheel_bids_app_toolkit) in the BIDS Client. These modules provide features to help set up the data to run on, call the BIDS App command, and then get the results into the output folder. If you create a method specifically for your gear, consider whether other BIDS Apps gears may be able to use it and [submit a merge request](https://gitlab.com/flywheel-io/public/bids-client/-/merge_requests) to contribute to ongoing improvements for the BIDS App toolkit. By using this centralized toolkit, we intend to keep the BIDS App gears in close parity and provide high test coverage to reduce errors and dev time.

This template was created specifically for gears that run on BIDS formatted data, but it can be a good start to any gear. The file manifest.json provides examples of inputs, configuration parameters, and references. After running the tests, this template can be uploaded as is with `fw-beta gear upload` and will run as a gear.


## Edit below this point to create the README for your gear.
# {{gear_name}} ({{gear_label}})

## Overview

*{Link To Usage}*

*{Link To FAQ}*

### Summary

*{From the "description" section of the manifest}*

### Cite

*{From the "cite" section of the manifest}*

### License

*License:* *{From the "license" section of the manifest. Be as specific as possible,
specifically when marked as Other.}*

### Classification

*Category:* *{From the "custom.gear-builder.category" section of the manifest}*

*Gear Level:*

- [ ] Project
- [ ] Subject
- [ ] Session
- [ ] Acquisition
- [ ] Analysis

----

[[_TOC_]]

----

### Inputs

- archived_runs
    - OPTIONAL
    - __Type__: file
    - If there are other files that will allow the algorithm to run or pick up analysis 
      from a certain point, then the archive can be supplied here. This zip will be
      automatically unzipped within the gear. NOTE: When the BIDSAppContext is 
      created, if this field is present (as "archived_runs"), the toolkit will unzip 
      the attached file into the BIDS directory and update paths appropriately. See 
      BIDSAppContext.check_archived_inputs for more details.

### Config

- bids_app_command
    - OPTIONAL
    - __Type__: free-text
    - The gear will run the defaults for the algorithm without anything in this box. If
      you wish to provide the command as you would on a CLI, input the exact command
      here. Flywheel will automatically update the BIDS_dir, output_dir, and
      analysis_level. (
      e.g., `bids_app bids_dir output participant --valid-arg1 --valid-arg2`)
    - For more help to build the command, please see REPLACE:<BIDS App documentation>.
    - Note: If you use a kwarg here, don't worry about putting a value in the box for
      the same kwarg below. Any kwarg that you see called out in the config UI and that
      is given a value will supersede the kwarg:value given as part of the
      bids_app_command.
  
- debug
    - __Type__: Boolean
    - __Default__: false
    - Verbosity of log messages; default results in INFO level, True will log DEBUG level 

- gear-dry-run
    - __Type__: Boolean
    - __Default__: false (set to true for template)
    - Do everything related to Flywheel except actually execute BIDS App command.
    - Note: This is NOT the same as running the BIDS app command with `--dry-run`. gear-dry-run will not actually download the BIDS data, attempt to run the BIDS app command, or do any metadata/result updating.

- gear-post-processing-only
    - __Type__: Boolean
    - __Default__: false
    - If an archive file is available, one can pick up with a previously analyzed
      dataset. This option is useful if the metadata changed (or needs to change). It is also useful for development.

__DEVELOPERS__, the following fields are *optional*. You may keep them in the manifest 
or remove them. They are provided in this template so that you know the 
functionalities exist.
- gear-intermediate-files (optional)
  - __Type__: string
  - Space separated list of FILES to retain from the intermediate work directory.
- gear-intermediate-folders (optional)
   - __Type__: string
   - Space separated list of FOLDERS to retain from the intermediate work directory.
- gear-expose-all-outputs (optional)
    - __Type__: Boolean
    - __Default__: false
    - Keep ALL the extra output files that are created during the run in addition to the normal, zipped output. Note: This option may cause a gear failure because there are too many files for the engine.
- gear-save-intermediate-output (optional)
    - __Type__: Boolean
    - __Default__: false
    - Gear will save ALL intermediate output into {{bids_app_binary}}_work.zip

### Custom
- analysis-level
  - For all intents and purposes, the current analysis-level should remain 
    "participant". The BIDSAppContext tries to determine if the gear was launched 
    from the project level (and would update to "group"), but most applications 
    should remain "participant" for now. This field is used to populate the BIDS App 
    command with the proper participant/group arg.
  
- bids-app-binary
  - Update this field's value to the name of the algorithm as it would be called from 
    the commandline. (e.g., qsiprep)
  
- bids-app-data-types
  - The types of data to process. If an algorithm does not use a specific type, remove 
    the corresponding name of the BIDS directory from this list to exclude that type of 
    data from download during `export_bids`.
  
- license dependencies
  - Add additional dependencies as the algorithm requires. Common dependencies are 
    FreeSurfer, ANTS, FSL
  
### Outputs

#### Files

*{A list of output files (if possible)}*

bids_tree.html

- A list of all BIDS formatted files and folders available as input to the BIDS App

app_template_{algorithm}_{destination_container}.zip

- Output files from the algorithm

{algorithm}_log.txt

- Log from the {algorithm} algorithm

job.log

- Details on the gear execution on Flywheel

- *{Output-File}*
    - __Name__: *{From "outputs.Input-File"}*
    - __Type__: *{From "outputs.Input-File.base"}*
    - __Optional__: *{From "outputs.Input-File.optional"}*
    - __Classification__: *{Based on "outputs.Input-File.base"}*
    - __Description__: *{From "outputs.Input-File.description"}*
    - __Notes__: *{Any additional notes to be provided by the user}*

#### Metadata

Any notes on metadata created by this gear

### Pre-requisites

This section contains any prerequisites

#### Prerequisite Gear Runs

BIDS curation must have been successful so that files will be written to disk in 
valid BIDS format when this gear is run.

A list of gears, in the order they need to be run:

1. __*{Gear-Name}__*
    - Level: *{Level at which gear needs to be run}*

#### Prerequisite Files

A list of any files (OTHER than those specified by the input) that the gear will need.
If possible, list as many specific files as you can:

1. ____{File-Name}__*
    - Origin: *{Gear-Name, or Scanner, or Upload?}*
    - Level: *{Container level the file is at}*
    - Classification: *{Required classification(s) that the file can be}*

#### Prerequisite Metadata

A description of any metadata that is needed for the gear to run.
If possible, list as many specific metadata objects that are required:

1. BIDS
    - {container}.info.BIDS
    - Enables files to be written to disk in BIDS format

1. __*{Metadata-Key}__*
    - Location: *{Nested Metadata Location (info.object1, age, etc.)}*
    - Level: *{Container level that metadata is at}*

## Usage

REPLACE: This section provides a more detailed description of the gear, including not
just WHAT it does, but HOW it works in flywheel.

app_template will run with very little intervention from the user. After selecting a
session and clicking "Run Gear", selections may be made from the GUI to change
default behavior or the user may start the gear without changing anything.

Previous processing that app_template would normally look for in the BIDS directory may be
supplied in the Inputs tab under "archived_runs". This file will be unzipped into
the BIDS working directory.

In the Configuration tab, one may provide the commandline input you would run in a
terminal for app_template in the free-text bids_app_command field. The BIDS App Toolkit parses
any arguments beyond the typical `app_name BIDS_dir output_dir
analysis_level` when creating the BIDSAppContext object (see flywheel_bids.
flywheel_bids_app_toolkit.context.parse_bids_app_commands), so additional
commandline arguments will be passed to the algorithm. Additionally, the gear checks
the arguments against the app_template usage file early in the gear run to fail quickly, if
an errant argument is provided.


### Description

*{A detailed description of how the gear works}*
- The options from the GUI are turned into a BIDSAppContext, storing all the relevant
  options and files.
- IF the bids_app_command is populated, the command is parsed and checked against
  the app_template usage file. Extraneous or errant args will cause immediate gear failure.
- Flywheel details are collected for provenance and naming.
- A shell command is generated and cleaned, then passed to the BIDS algorithm.
- Results are parsed and updated as necessary to be zipped inside of
  /flywheel/v0/output upon conclusion of the gear run.
- The gear status is reported.


### Workflow

A picture and description of the workflow

```mermaid
graph LR;
    A[Input-File]:::input --> C;
    C[Upload] --> D[Parent Container <br> Project, Subject, etc];
    D:::container --> E((Gear));
    E:::gear --> F[Analysis]:::container;
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow

1. Upload file to container
1. Select file as input to gear
1. Gear places output in Analysis

### Use Cases

This section is very gear dependent, and covers a detailed walkthrough of some use
cases. Should include Screenshots, example files, etc.

#### Use Case 1

__*Conditions__*:

- *{A list of conditions that result in this use case}*
- [ ] Possibly a list of check boxes indicating things that are absent
- [x] and things that are present

*{Description of the use case}*

### Logging

Debug is the highest number of messages and will report some locations/options and all
errors. If unchecked in the configuration tab, only INFO and higher priority level log
messages will be reported.

## FAQ

[FAQ.md](FAQ.md)

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]
<!-- markdownlint-disable-file -->
