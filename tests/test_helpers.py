from unittest.mock import patch

import pytest

from fw_gear_bids_app_template.utils.helpers import validate_setup


@patch("fw_gear_bids_app_template.utils.helpers.validate_kwargs")
def test_validate_setup(mock_validate_kwargs, mock_context, mock_app_context):
    mock_app_context.bids_app_options = {"oooo": "pick_me"}
    validate_setup(mock_context, mock_app_context)
    mock_validate_kwargs.assert_called_once_with(mock_app_context)
