"""Utilities that are called inside tests."""

import json
from pathlib import Path


def change_path_for_testing(gear_context, config_path):
    """Change the path in the gear context from /flywheel/v0 to wherever we are running (for testing)

    Args:
        gear_context (GearToolkitContext): The gear context
        config_path (Path): The path to the gear's config.json
    """
    for name in gear_context.config_json["inputs"]:
        if gear_context.config_json["inputs"][name]["base"] == "file":
            ending = gear_context.config_json["inputs"][name]["location"]["path"].split("/")[3:]
            gear_context.config_json["inputs"][name]["location"]["path"] = str(config_path / Path(*ending))
